import React, { Component } from 'react';
import Home from './Home';
import Enterprises from './Enterprises';

class Search extends Component {
    
    state = {
        searchBar: false,
    }

    showSearch = () => {
        let newstate = !this.state.searchBar;
        this.setState({ searchBar: newstate});
    }

    render() {
        return (
            <div className="search">
                <Home showSearch={this.showSearch} search={this.state.searchBar}/>
                {!this.state.searchBar ? 
                <h2 className="text-center" id="pesquisa">Clique na busca para iniciar.</h2> 
                : <Enterprises/>}
            </div>
        );
    }
}
export default Search;

