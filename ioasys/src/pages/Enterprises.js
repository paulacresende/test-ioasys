import React from 'react';
import E1 from '../assets/img-e-1.png';
import { Link } from 'react-router-dom';
import "./styles/Enterprises.css";

const Enterprises = () => {
    return (
            <div className="enterprises" >
                <div className="img">
                    <Link to="/enterprise"><img src={E1} alt="enterprise"/> </Link>
                </div>
                <div className="enterprises-data">
                <Link to="/enterprise"><h1>Empresa1</h1></Link>
                    <h3>Negócio</h3>
                    <h5>Brasil</h5>
                </div>
            </div>
    )
}

export default Enterprises