import React, { Component } from 'react';
import logo from '../assets/logo-home.png';
import { withRouter } from "react-router-dom";
import api from "../services/api";
import { login } from "../services/auth";
import './styles/Login.css';

class Login extends Component {
  state = {
    email: "",
    password: "",
    apiHeaders: {},
    error: ""
  };

  handleLogin = async e => {
    e.preventDefault();
    const { email, password } = this.state;
    if (!email || !password) {
      this.setState({ error: "Preencha e-mail e senha para continuar!" });
    } else {
      try {
        const response = await api.post("/users/auth/sign_in", { email, password },
          {
            headers: {
              'Content-Type': 'application/json',
            }
          }
        );
        login(response.data.token);
        this.props.history.push("/home");
        const apiHeaders = response.headers;
        this.setState({ apiHeaders });
      } catch (err) {
        this.setState({
          error:
            "Usuário não registrado"
        });
      }
      
    }

  };

render() {
  return (
    <div className="container-fluid">
      <form onSubmit={this.handleLogin}>
        <img src={logo} alt="logo-ioasys" />
        {this.state.error && <p>{this.state.error}</p>}
        <h1>BEM VINDO AO EMPRESAS</h1>
        <p>Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</p>
        <input
          type="email" id = "icon-email"
          placeholder="E-mail"
          onChange={e => this.setState({ email: e.target.value })}
        />
        <hr id = "line"/>
        <input id = "icon-pass"
          type="password"
          placeholder="Senha"
          onChange={e => this.setState({ password: e.target.value })}
        />
        <hr id="line"/>
        <button type="submit">ENTRAR</button>
      </form>
    </div>

  );
}

}

export default withRouter(Login);

