import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../assets/logo-nav.png';
import lupa from '../assets/ic-search-copy.png';
import close from '../assets/ic-close.png';
import "./styles/Home.css";

const Home = (props) => {

  let search = (<><Link to="/" >
    <img src={logo} alt="logo" id="logo-header" />
  </Link>
    <img src={lupa} alt="lupa" onClick={props.showSearch} /></>)

  if (props.search) {
    search = (
      <div>
        <img src={lupa} alt="lupa" />
        <input type="text" placeholder="Pesquisar" id="search" />
        <img src={close} alt="fechar" onClick={props.showSearch} />
      </div>
    )
  }

  return (
      <nav className="navbar">
        {search}
      </nav>
  )
}

export default Home;