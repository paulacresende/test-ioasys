import React from 'react';
import E1 from '../assets/img-e-1-lista.png';
import Home from './Home';
import './styles/Company.css';

const Company = () => {
    return (
        <div>
            <div>
                <Home />
            </div>
            <div className="box">
                <div className="box-secundary">
                    <div>
                        <img src={E1} alt="enterprise" />
                    </div>
                    <div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                </div>

            </div>

        </div>
    )
}

export default Company