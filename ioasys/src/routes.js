import React from 'react';

import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'
import Login from './pages/Login';
import Search from './pages/Search';
import Company from './pages/Company';


import { isAuthenticated } from "./services/auth";

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
        {...rest}
        render={props =>
            isAuthenticated() ? (
                <Component {...props} />
            ) : (
                    <Redirect to={{ pathname: "/", state: { from: props.location } }} />
                    
                )
        }
    />
);

const Routes = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component = {Login} />
            <PrivateRoute path="/home" component = {Search} />
            <Route path="/enterprise" component = {Company}/>
        </Switch>
    </BrowserRouter>
);
export default Routes;